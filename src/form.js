'use strict';

var React = require('react/addons');
var cx = React.addons.classSet;

module.exports = React.createClass({
  propTypes: {
    children: React.PropTypes.arrayOf(React.PropTypes.element).isRequired
  },
  getDefaultProps: function () {
    return {
      onSubmit: function () {}
    }
  },
  getInitialState: function () {
    return {
      models: this.props.schema.models,
      valid: false
    };
  },
  handleSchemaChange: function () {
    this.setState({
      models: this.props.schema.models,
      valid: this.props.schema.valid
    });
  },
  componentWillMount: function () {
    this.props.schema.addListener('change', this.handleSchemaChange);
  },
  componentWillUnMount: function () {
    this.props.schema.removeListener('change', this.handleSchemaChange);
  },
  handleChange: function (name, value) {
    this.props.schema.update(name, value);
  },
  handleSubmit: function (e) {
    e.preventDefault();

    // we should have some sort of prop that allows
    // us to pass our valid fields as a hash
    this.props.onSubmit()

    return;
  },
  render: function () {
    var defaultClasses = {
      'luther-form': true
    };

    var className = cx(defaultClasses, this.props.className);
    var children = React.Children.map(this.props.children, function (child) {
      // very simplistic check to see if child is a component
      // that conforms to the InputComponent prop signature
      if (child.props.model)  {
        var model = this.state.models[child.props.model];

        return React.addons.cloneWithProps(child, {
            value: model.value,
            valid: model.valid,
            handleChange: this.handleChange
        });
      }

      return child;
    }, this);

    return (
      <form onSubmit={this.handleSubmit} className={className}>
        {children}
      </form>
    );
  }
});
