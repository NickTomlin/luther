'use strict';

var React = require('react/addons');
var cx = React.addons.classSet;
var assign = require('object-assign');

// base element of form, provides a discrete wrapper
// for input. Should probably be configurable
//
// TODO:
// this should be a catch all wrapper
// that select/input classes import and use
// to wrap their own custom functionality
// it should be agnostic as far as the type of input
// is concerned
module.exports = React.createClass({
  getInitialState: function () {
    return {
      showStatus: false
    };
  },
  handleFocus: function () {
    this.setState({
      showStatus: false
    })
  },
  handleBlur: function () {
    this.setState({
      showStatus: true
    })
  },
  onChange: function () {
    if (this.props.handleChange) {
      var value = this.refs.input.getDOMNode().value;
      this.props.handleChange(this.props.model, value);
    }
  },
  render: function () {
    var validationClasses;
    var valid = this.props.valid;
    var defaultClasses = {
      'luther-input-group': true
    };

    if (this.state.showStatus) {
      validationClasses = {
        'is-valid': valid,
        'is-invalid': !valid
      };
    }

    var classes = cx(
      defaultClasses,
      this.props.className,
      validationClasses
    );

    return (
      <label className={classes}>
        <strong>{this.props.children}</strong>&nbsp;
        <input
          type="text"
          ref="input"
          value={this.props.value}
          name={this.props.model}
          onChange={this.onChange}
          onBlur={this.handleBlur}
          onFocus={this.handleFocus}
          />
      </label>
    )
  }
});
