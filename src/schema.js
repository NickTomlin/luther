'use strict';

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

// temporary...
var validators = {
  email: {
    test: function (value) {
      return /[.\w]+@{1}(.*)\.{1}/.test(value);
    },
    message: 'invalid email'
  },
  number: {
    test: function (value) {
      return /^\d+$/.test(value);
    },
    message: 'not a number'
  }
};

// a "store" of some sort
// TODO: handle arrays as well as objects?
function Schema (data) {
  this.models = {};
  this._data = data;
  this.valid = true;

  this._initializeModels(this._data);
}

assign(Schema.prototype, EventEmitter.prototype);

Schema.prototype._initializeModels = function (data) {
  for (var name in data) {
    var model = data[name];

    this.models[name] = {
      value: model.value,
      valid: true,
      validators: model.validate || [],
      required: model.required === false ? false : true,
      messages: []
    }
  }

  this.validate();
};

Schema.prototype.reset = function () {
  this.valid = true;
  this._initializeModels(this._data);
  this._emitChange();
};

Schema.prototype.validate = function () {
  this.valid = Object.keys(this.models).every(function (modelName) {
    var model = this.models[modelName];

    if (!model.required) {
      return true;
    }

    return this._validateModel(model);
  }.bind(this));

  return this.valid;
};

Schema.prototype.update = function (modelName, updatedValue) {
  if (!modelName in this.models) throw new Error('Invalid model specified');

  var model = this.models[modelName];
  model.value = updatedValue;

  // this is not performant
  // but simplifies who owns validation
  this.validate();

  this._emitChange('change');
};

Schema.prototype._emitChange = function () {
  this.emit('change');
};

Schema.prototype._validateModel = function (model) {
  var isValid;
  var messages = [];
  var value = model.value;

  if (!value) {
    messages = ['Is required'];
    isValid = false;
  } else {
    isValid = model.validators.every(function (validatorName) {
      var validator = validators[validatorName];
      var valid = validator.test(model.value);

      if (!valid) {
        messages.push(validators.message)
      }

      return valid;
    });
  }

  model.valid = isValid;
  model.messages = messages;

  return isValid;
}

Schema.prototype.serialize = function () {
  return Object.keys(this.models).reduce(function (obj, modelName) {
    var model =  this.models[modelName];
    obj[modelName] = model.value;

    return obj;
  }.bind(this), {});
};

module.exports = Schema;
