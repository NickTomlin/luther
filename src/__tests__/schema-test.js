'use strict';

jest.dontMock('../schema');

describe('schema', function () {
  var Schema, data;

  beforeEach(function () {
    Schema = require('../schema');

    data = {
      firstName: {
        value: 'bobbins'
      },
      lastName: {
        value: 'bibbins',
        required: false
      },
      phone: {
        validate: ['number']
      }
    };
  });

  it('takes an object and translates into an internal model collection', function () {
    var schema = new Schema(data)

    expect(Object.keys(schema.models).length).toEqual(3);
  });

  it('serializes model values', function () {
    var schema = new Schema(data);

    var keyPairs = schema.serialize();

    expect(keyPairs.firstName).toEqual(data.firstName.value);
    expect(keyPairs.lastName).toEqual(data.lastName.value);
    expect(keyPairs.phone).toEqual(data.phone.value);
  });

  describe('update', function () {
    var schema;

    beforeEach(function () {
      schema = new Schema(data);
    });

    it('updates model values', function () {
      var newValue = 'fibbins';
      schema.update('firstName', newValue);

      expect(schema.models.firstName.value).toEqual(newValue);
    });

    it('broadcasts a change event when a model is updated', function () {
      var newValue = 'fibbins';
      schema.update('firstName', newValue);
      schema.update('lastName', newValue);

      var calls = schema.emit.mock.calls.length;

      expect(calls).toEqual(2);
    });

    it('validates models on update', function () {
      schema.validate = jest.genMockFn();
      schema.update('firstName', 'test');

      expect(schema.validate).toBeCalled();
    })
  });

  describe('validate', function () {
    var validData;

    beforeEach(function () {
      validData = {
        num: {
          value: '1234',
          validate: ['number']
        },
        name: {
          value: 'foo'
        },
        optional: {
          value: 'optional',
          required: false
        }
      };

    });

    it('returns true if models are valid', function () {
      var schema = new Schema(validData);

      expect(schema.validate()).toBeTruthy();
    });

    it('returns false if a required model is null', function () {
      var schema = new Schema(data);

      schema.models.firstName.value = undefined;

      expect(schema.validate()).toBeFalsy()
    });

    it('adds validation messages to models', function () {
      var schema = new Schema(data);

      schema.models.firstName.value = undefined;
      schema.validate();

      expect(schema.models.firstName.valid).toBeFalsy();
      expect(schema.models.firstName.messages.length).toEqual(1);
    });

    it('sets the value of the schema to false if model is invalid', function () {
      var schema = new Schema(data);

      schema.models.phone.value = 'not a phone';
      schema.validate();

      expect(schema.valid).toBeFalsy()
    });

    it('does not return false for non required models that are null', function () {
      delete validData.optional.value;
      var schema = new Schema(validData);
      schema.validate();
      expect(schema.valid).toBeTruthy();
    });
  });

  describe('reset', function () {
    it('returns models to original state', function () {
      var schema = new Schema(data);

      expect(schema.serialize().firstName).toEqual(data.firstName.value);

      schema.update('firstName', 'changed');
      expect(schema.serialize().firstName).toEqual('changed');

      schema.reset();
      expect(schema.serialize().firstName).toEqual(data.firstName.value);
    });

    it('emits a change event', function (done) {
      var schema = new Schema(data);
      schema.emit = jest.genMockFn();

      expect(schema.emit.mock.calls.length).toEqual(0);
      schema.reset();
      expect(schema.emit.mock.calls.length).toEqual(1);
    });
  });
});
