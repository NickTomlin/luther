'use strict';
jest.dontMock('../form');

xdescribe('Form', function () {
  var Form, Schema;
  var React = require('react/addons');
  var TestUtils = React.addons.TestUtils;
  var render = TestUtils.renderIntoDocument;

  beforeEach(function () {
    Form = require('../form');
    Schema = require('../schema');
  });

  it('renders a form', function () {
    var schema = new Schema();

    var form = render(<Form schema={schema} />);
    expect(form.getDOMNode().tagName).toEqual('form');
  });

  it('does not blow up when children are text components');
});
