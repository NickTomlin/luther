'use strict';

var React = require('react');
var Luther = require('../index');
var Schema = Luther.Schema;
var Form = Luther.Form;
var Input = Luther.Input;

var modelData = {
  name: {},
  favoriteNumber: {
    // providing a value causes issuezzz
    value: 1234,
    validate: ['number'],
  },
  optionalFavoriteColor: {
    vaue: '',
    required: false
  }
};

var schema = new Schema(modelData);

var App = React.createClass({
  handleFormSubmit: function () {
    console.log('Top Level Submit handler', schema.serialize());
  },
  render: function () {

    return (
      <div>
        <p>form handles validation</p>
        <Form schema={schema} onSubmit={this.handleFormSubmit}>
          <Input model="name">Name!</Input>
          <Input model="optionalFavoriteColor">(optional) Favorite Color</Input>
          <Input model="favoriteNumber">Favorite Number!</Input>

          <label htmlFor="foo">
            Non Managed input
            <input name="foo" type="text" />
          </label>
        </Form>
      </div>
    )
  }
});

React.render(<App />, document.body);
