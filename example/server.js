'use strict';

// simulate a server

module.exports = {
  post: function (data, cb) {
    this.request(data, cb);
  },
  get: function (data, cb) {
    this.request(data, cb);
  },
  request: function (data, cb) {
    setTimeout(function () {
      if (data.validity) {
        cb(true);
      } else {
        cb(false)
      }
    }, 100);
  }
};
